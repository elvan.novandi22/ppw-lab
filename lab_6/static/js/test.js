$( document ).ready(function() {
    var button_8 = $('button:contains("8")');
    var button_4 = $('button:contains("4")');
    var button_9 = $('button:contains("9")');
    var button_0 = $('button:contains("0")');
    var button_1 = $('button:contains("1")');


    var button_add = $('button:contains("+")');
    var button_sub = $('button:contains("-")');
    var button_mul = $('button:contains("*")');
    var button_div = $('button:contains("/")');

    var button_clear = $('button:contains("AC")');
    var button_res = $('button:contains("=")');
    var button_SIN = $('button:contains("SIN")');
    var button_LOG = $('button:contains("LOG")');
    var button_TAN = $('button:contains("TAN")');

    QUnit.test( "Addition Test", function( assert ) {
      button_8.click();
      button_add.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
      button_clear.click();
    });
    QUnit.test( "Substraction Test", function( assert ) {
      button_8.click();
      button_sub.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 12, "8 - 4 must be 4" );
      button_clear.click();
    });QUnit.test( "Multiply Test", function( assert ) {
      button_8.click();
      button_mul.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 32, "8 * 4 must be 32" );
      button_clear.click();
    });

    QUnit.test( "Division Test", function( assert ) {
      button_8.click();
      button_div.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 2, "8 / 4 must be 2" );
      button_clear.click();
    });
    QUnit.test( "SIN Test", function( assert ) {
      button_9.click();
      button_0.click();
      button_SIN.click();
      assert.equal( $('#print').val(), 1, "SIN(90) must be 1" );
      button_clear.click();
    });
    QUnit.test( "TAN Test", function( assert ) {
      button_0.click();
      button_TAN.click();
      assert.equal( $('#print').val(), 0, "TAN(0) must be 0" );
      button_clear.click();
    });
    QUnit.test( "LOG Test", function( assert ) {
      button_1.click();
      button_0.click();
      button_0.click();
      button_LOG.click();
      assert.equal( $('#print').val(), 2, "LOG(100) must be 2" );
      button_clear.click();
    });


});
